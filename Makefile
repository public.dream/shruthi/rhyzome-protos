all: rhyzome_protos/libvirt.pb.go rhyzome_protos/openwrt.pb.go

rhyzome_protos/%.pb.go: rhyzome_protos/%.proto
	protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative rhyzome_protos/*.proto

rhyzome_libvirt_protos/rhyzome_libvirt_grpc.pb.go rhyzome_libvirt_protos/rhyzome_libvirt.pb.go: rhyzome_libvirt_protos/rhyzome_libvirt.proto
	protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative rhyzome_libvirt_protos/rhyzome_libvirt.proto
