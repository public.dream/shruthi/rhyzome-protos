// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.12.4
// source: rhyzome_protos/libvirt.proto

package rhyzome_protos

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// RhyzomeLibvirtClient is the client API for RhyzomeLibvirt service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type RhyzomeLibvirtClient interface {
	EventStream(ctx context.Context, opts ...grpc.CallOption) (RhyzomeLibvirt_EventStreamClient, error)
	InstanceStarted(ctx context.Context, in *InstanceStartedRequest, opts ...grpc.CallOption) (*InstanceStartedResponse, error)
	InstanceStopped(ctx context.Context, in *InstanceStoppedRequest, opts ...grpc.CallOption) (*InstanceStoppedResponse, error)
}

type rhyzomeLibvirtClient struct {
	cc grpc.ClientConnInterface
}

func NewRhyzomeLibvirtClient(cc grpc.ClientConnInterface) RhyzomeLibvirtClient {
	return &rhyzomeLibvirtClient{cc}
}

func (c *rhyzomeLibvirtClient) EventStream(ctx context.Context, opts ...grpc.CallOption) (RhyzomeLibvirt_EventStreamClient, error) {
	stream, err := c.cc.NewStream(ctx, &RhyzomeLibvirt_ServiceDesc.Streams[0], "/protos.RhyzomeLibvirt/EventStream", opts...)
	if err != nil {
		return nil, err
	}
	x := &rhyzomeLibvirtEventStreamClient{stream}
	return x, nil
}

type RhyzomeLibvirt_EventStreamClient interface {
	Send(*LibvirtEventUpdate) error
	Recv() (*LibvirtEvent, error)
	grpc.ClientStream
}

type rhyzomeLibvirtEventStreamClient struct {
	grpc.ClientStream
}

func (x *rhyzomeLibvirtEventStreamClient) Send(m *LibvirtEventUpdate) error {
	return x.ClientStream.SendMsg(m)
}

func (x *rhyzomeLibvirtEventStreamClient) Recv() (*LibvirtEvent, error) {
	m := new(LibvirtEvent)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *rhyzomeLibvirtClient) InstanceStarted(ctx context.Context, in *InstanceStartedRequest, opts ...grpc.CallOption) (*InstanceStartedResponse, error) {
	out := new(InstanceStartedResponse)
	err := c.cc.Invoke(ctx, "/protos.RhyzomeLibvirt/InstanceStarted", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *rhyzomeLibvirtClient) InstanceStopped(ctx context.Context, in *InstanceStoppedRequest, opts ...grpc.CallOption) (*InstanceStoppedResponse, error) {
	out := new(InstanceStoppedResponse)
	err := c.cc.Invoke(ctx, "/protos.RhyzomeLibvirt/InstanceStopped", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RhyzomeLibvirtServer is the server API for RhyzomeLibvirt service.
// All implementations must embed UnimplementedRhyzomeLibvirtServer
// for forward compatibility
type RhyzomeLibvirtServer interface {
	EventStream(RhyzomeLibvirt_EventStreamServer) error
	InstanceStarted(context.Context, *InstanceStartedRequest) (*InstanceStartedResponse, error)
	InstanceStopped(context.Context, *InstanceStoppedRequest) (*InstanceStoppedResponse, error)
	mustEmbedUnimplementedRhyzomeLibvirtServer()
}

// UnimplementedRhyzomeLibvirtServer must be embedded to have forward compatible implementations.
type UnimplementedRhyzomeLibvirtServer struct {
}

func (UnimplementedRhyzomeLibvirtServer) EventStream(RhyzomeLibvirt_EventStreamServer) error {
	return status.Errorf(codes.Unimplemented, "method EventStream not implemented")
}
func (UnimplementedRhyzomeLibvirtServer) InstanceStarted(context.Context, *InstanceStartedRequest) (*InstanceStartedResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method InstanceStarted not implemented")
}
func (UnimplementedRhyzomeLibvirtServer) InstanceStopped(context.Context, *InstanceStoppedRequest) (*InstanceStoppedResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method InstanceStopped not implemented")
}
func (UnimplementedRhyzomeLibvirtServer) mustEmbedUnimplementedRhyzomeLibvirtServer() {}

// UnsafeRhyzomeLibvirtServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to RhyzomeLibvirtServer will
// result in compilation errors.
type UnsafeRhyzomeLibvirtServer interface {
	mustEmbedUnimplementedRhyzomeLibvirtServer()
}

func RegisterRhyzomeLibvirtServer(s grpc.ServiceRegistrar, srv RhyzomeLibvirtServer) {
	s.RegisterService(&RhyzomeLibvirt_ServiceDesc, srv)
}

func _RhyzomeLibvirt_EventStream_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(RhyzomeLibvirtServer).EventStream(&rhyzomeLibvirtEventStreamServer{stream})
}

type RhyzomeLibvirt_EventStreamServer interface {
	Send(*LibvirtEvent) error
	Recv() (*LibvirtEventUpdate, error)
	grpc.ServerStream
}

type rhyzomeLibvirtEventStreamServer struct {
	grpc.ServerStream
}

func (x *rhyzomeLibvirtEventStreamServer) Send(m *LibvirtEvent) error {
	return x.ServerStream.SendMsg(m)
}

func (x *rhyzomeLibvirtEventStreamServer) Recv() (*LibvirtEventUpdate, error) {
	m := new(LibvirtEventUpdate)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _RhyzomeLibvirt_InstanceStarted_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(InstanceStartedRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RhyzomeLibvirtServer).InstanceStarted(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protos.RhyzomeLibvirt/InstanceStarted",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RhyzomeLibvirtServer).InstanceStarted(ctx, req.(*InstanceStartedRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RhyzomeLibvirt_InstanceStopped_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(InstanceStoppedRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RhyzomeLibvirtServer).InstanceStopped(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/protos.RhyzomeLibvirt/InstanceStopped",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RhyzomeLibvirtServer).InstanceStopped(ctx, req.(*InstanceStoppedRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// RhyzomeLibvirt_ServiceDesc is the grpc.ServiceDesc for RhyzomeLibvirt service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var RhyzomeLibvirt_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "protos.RhyzomeLibvirt",
	HandlerType: (*RhyzomeLibvirtServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "InstanceStarted",
			Handler:    _RhyzomeLibvirt_InstanceStarted_Handler,
		},
		{
			MethodName: "InstanceStopped",
			Handler:    _RhyzomeLibvirt_InstanceStopped_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "EventStream",
			Handler:       _RhyzomeLibvirt_EventStream_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
	},
	Metadata: "rhyzome_protos/libvirt.proto",
}
